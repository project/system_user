<?php

namespace Drupal\system_user\Service;

/**
 * SystemUser service.
 *
 * @package system_user
 * @deprecated Use SystemUserManager instead
 */
class SystemUser extends SystemUserManager {}
